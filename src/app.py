# -*- coding: utf-8 -*-
from flask import Flask, request, render_template
from utils.oauth1_server import db, Client, create_authorization_server
from authlib.oauth1.errors import OAuth1Error
from lti1p0 import LTILaunchForm
from lti1p0.errors import LTIError

USE_CACHE = False

# Create Flask App

app = Flask(__name__)
app.debug = True
app.config.update({
    'OAUTH1_SUPPORTED_SIGNATURE_METHODS': ['HMAC-SHA1', 'HMAC-SHA256'],
    'SQLALCHEMY_TRACK_MODIFICATIONS': False,
    'SQLALCHEMY_DATABASE_URI': 'sqlite://'
})

ctx = app.app_context()
ctx.push()

# Initialize Oauth1 database (This is a SQLAlchemy instance with SQLite)
db.init_app(app)
db.create_all()

# Create Hub Educacional Client Example
hub_client_example = Client(
    client_id='hub.educacional.com',
    client_secret='secret',
)

# Persist Hub client to Client Database
db.session.add(hub_client_example)
db.session.commit()

# Create Oauth1 Server
server = create_authorization_server(app, USE_CACHE, USE_CACHE)

# Setup Basic LTI /launch endpoint
@app.route('/launch', methods=['POST'])
def launch():
    verified = False
    error_title = error_message = None
    try:
        # Verify OAuth1 request
        oauth1_request = server.create_oauth1_request(request)
        server.validate_temporary_credentials_request(oauth1_request)

        # Verify LTI 1.0 Basic Launch
        launch = LTILaunchForm(request.form)
        LTILaunchForm.validate_lti_launch(launch)

        # If OAuth1 and LTI 1.0 Basic Launch validation return no error,
        # the launch is verified. Once verified you can now prepare resource
        # to return to user.
        #   For example:
        #   * Create session for user
        #   * Redirect user to resource page
        #   * i.e. class, course, assignment page, etc.
        #
        verified = True
    except (OAuth1Error, LTIError) as error:
        if not error.description and not error.get_error_description():
            error_title = 'Error'
            error_message = error.error
        else:
            error_title = error.error
            error_message = error.description

    # Prepare params to render
    params_string = ''
    for item in sorted(request.form.items()):
        params_string += item[0] + ':' + item[1] + '\n'
    print('Message Parameters:', params_string, sep='\n')
    print('verified:', verified)
    print('error_title:', error_title)
    print('error_message:', error_message)
    if verified:
        status_code = 200
    else:
        status_code = 401
    # Render Resource
    return render_template(
        'message_params.html',
        params=params_string,
        verified=verified,
        error_title=error_title,
        error_message=error_message
    ), status_code
