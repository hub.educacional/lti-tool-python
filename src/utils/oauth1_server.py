# File: authlib/tests/flask/test_oauth1/oauth1_server.py
# Source: https://github.com/lepture/authlib

import os
from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from authlib.flask.oauth1 import (
    AuthorizationServer
)
from authlib.flask.oauth1.sqla import (
    OAuth1ClientMixin,
    OAuth1TokenCredentialMixin,
    OAuth1TemporaryCredentialMixin,
    OAuth1TimestampNonceMixin,
    create_query_client_func,
    register_authorization_hooks
)
from authlib.flask.oauth1.cache import (
    register_temporary_credential_hooks,
    register_nonce_hooks
)
from authlib.oauth1.errors import OAuth1Error
from .cache import SimpleCache
os.environ['AUTHLIB_INSECURE_TRANSPORT'] = 'true'

import hmac
import hashlib
import binascii
from authlib.common.encoding import to_bytes
from authlib.oauth1.rfc5849 import signature
from authlib.oauth1.rfc5849.util import escape

def verify_hmac_sha256(request):
    text = signature.generate_signature_base_string(request)

    key = escape(request.client_secret or '')
    key += '&'
    key += escape(request.token_secret or '')

    sig = hmac.new(to_bytes(key), to_bytes(text), hashlib.sha256)
    return binascii.b2a_base64(sig.digest())[:-1]

AuthorizationServer.register_signature_method(
    'HMAC-SHA256', verify_hmac_sha256
)

db = SQLAlchemy()


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(40), unique=True, nullable=False)

    def get_user_id(self):
        return self.id


class Client(db.Model, OAuth1ClientMixin):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(
        db.Integer, db.ForeignKey('user.id', ondelete='CASCADE')
    )
    user = db.relationship('User')

    def get_rsa_public_key(self):
        return read_file_path('rsa_public.pem')


class TokenCredential(db.Model, OAuth1TokenCredentialMixin):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(
        db.Integer, db.ForeignKey('user.id', ondelete='CASCADE')
    )
    user = db.relationship('User')


class TemporaryCredential(db.Model, OAuth1TemporaryCredentialMixin):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(
        db.Integer, db.ForeignKey('user.id', ondelete='CASCADE')
    )
    user = db.relationship('User')


class TimestampNonce(db.Model, OAuth1TimestampNonceMixin):
    id = db.Column(db.Integer, primary_key=True)


def create_authorization_server(app, use_cache=False, lazy=False):
    query_client = create_query_client_func(db.session, Client)
    if lazy:
        server = AuthorizationServer()
        server.init_app(app, query_client)
    else:
        server = AuthorizationServer(app, query_client=query_client)
    if use_cache:
        cache = SimpleCache()
        register_nonce_hooks(server, cache)
        register_temporary_credential_hooks(server, cache)
        register_authorization_hooks(server, db.session, TokenCredential)
    else:
        register_authorization_hooks(
            server, db.session,
            token_credential_model=TokenCredential,
            temporary_credential_model=TemporaryCredential,
            timestamp_nonce_model=TimestampNonce,
        )
    return server