# -*- coding: utf-8 -*-

from .errors import (
    MissingRequiredParameterError,
    InvalidMessageTypeError,
    InvalidLTIVersionError,
    RequestFormTypeError
)

# Required LTI Basic Launch Parameters
# Here is the set of required parameters for basic launch.
# Basic Launch requires:
#   'lti_message_type'
#   'lti_version'
#   'resource_link_id'
#
PARAMETER_LTI_MESSAGE_TYPE = 'lti_message_type'
PARAMETER_LTI_VERSION = 'lti_version'
PARAMETER_RESOURCE_LINK_ID = 'resource_link_id'

REQUIRED_LTI_PARAMETERS = {
    PARAMETER_LTI_MESSAGE_TYPE,
    PARAMETER_LTI_VERSION,
    PARAMETER_RESOURCE_LINK_ID,
}

# LTI1.0 Launch:
# In LTI Basic Launch 'lti_version' parameter must be exactly 'LTI-1p0'
LTI_VERSION1 = 'LTI-1p0'

# LTI Message Types:
# This constant set lti_message_type supported by this implementation.
# For now, only basic launch will be implemented.
# Parameter (required): lti_message_type
LTI_BASIC_LAUNCH = 'basic-lti-launch-request'

IMPLEMENTED_LTI_MESSAGE_TYPES = {
    LTI_BASIC_LAUNCH,
}

class LTILaunchForm(dict):
    def __init__(self, request_form=None, validate=False):
        if request_form:
            try:
                super(LTILaunchForm, self).__init__(request_form)
            except (ValueError, TypeError) as error:
                raise RequestFormTypeError(request_form)
        else:
            super(LTILaunchForm, self).__init__()

        if validate:
            LTILaunchForm.validate_lti_launch(self)

    @classmethod
    def validate_lti_launch(cls, request_form):
        """Validate Basic LTI Launch"""
        # Required Parameter
        for required_parameter in REQUIRED_LTI_PARAMETERS:
            if not request_form.get(required_parameter):
                raise MissingRequiredParameterError(required_parameter)

        # lti_message_type=basic-lti-launch-request
        message_type = request_form[PARAMETER_LTI_MESSAGE_TYPE]
        if message_type != LTI_BASIC_LAUNCH:
            raise InvalidMessageTypeError()

        # lti_version=LTI-1p0
        message_version = request_form[PARAMETER_LTI_VERSION]
        if message_version != LTI_VERSION1:
            raise InvalidLTIVersionError()

        # resource_link_id
        # You may check if resource_link_id exists/are set
        return request_form
