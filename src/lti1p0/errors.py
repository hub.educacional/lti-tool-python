# -*- coding: utf-8 -*-

class BaseError(Exception):
    """Base Exception for all errors."""
    error = None
    description = ''

    def __init__(self, error=None, description=None):

        if error is not None:
            self.error = error
        if description is not None:
            self.description = description
        if self.description:
            message = '{}\n{}'.format(self.error, self.description)
        else:
            message = self.error

        super(BaseError, self).__init__(message)

    def __repr__(self):
        return '<{} "{}">'.format(self.__class__.__name__, self.error)

LTIError = BaseError

class GeneralError(BaseError):
    """General Exception for LTI errors."""
    error = 'general_lti_error'
    description = 'Something went wrong!'

    def __init__(self, *args, **kargs):
        message = '{}\n{}'.format(self.error, self.description)
        self._args = args
        self._kargs = kargs
        if args:
            message += '\n*args passed:{}'.format(args)
        if kargs:
            message += '\n**kargs passed:{}'.format(kargs)
        super(GeneralError, self).__init__(message)

    def get_args_kargs(self):
        return self._args, self._kargs
    def get_args(self):
        return self._args
    def get_kargs(self):
        return self._kargs

    def __repr__(self):
        return '<{} "{}">'.format(self.__class__.__name__, self.error)

class MissingRequiredParameterError(BaseError):
    """Missing required parameter in LTI message."""
    error = 'missing_required_parameter'
    def __init__(self, key):
        self._missing_parameter = key
        self.description = ('Missing "%(key)s" in parameters') % dict(key=key)
        super(MissingRequiredParameterError, self).__init__()
    def get_missing_parameter(self):
        return self._missing_parameter

class InvalidMessageTypeError(BaseError):
    """Invalid message type in LTI message."""
    error = 'invalid lti_message_type'

class InvalidLTIVersionError(BaseError):
    """Invalid message type in LTI message."""
    error = 'invalid lti_version'

class RequestFormTypeError(BaseError):
    error = 'request_form_type_error'
    def __init__(self, request_form):
        self.description = ('Couldn\'t create request form from object: {}'
            .format(request_form))
        self.description += (
            '\nExpecting a (key, value) mapping or iterator, or a dict. Found {}.'
            .format(request_form.__class__.__name__))

        super(RequestFormTypeError, self).__init__()
