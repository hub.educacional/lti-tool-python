# Hub Educacional - Exemplo de Ferramenta LTI (Python)

## Implementação (Provedor de Ferramenta LTI)

A implementação desse projeto deve como diretrizes os seguintes documentos: [Guia de implementação LTI](https://www.imsglobal.org/specs/ltiv1p1p1/implementation-guide), [Melhores práticas LTI](http://ltiapps.net/guide/LTI_Best_Practice.pdf) e o auxílio de [Inicialização Básica LTI: Parâmetros do POST](https://www.eduappcenter.com/docs/basics/post_parameters). Use esses documentos como referência para sua aplicação.

Esse exemplo foi escrito em Python + [Flask](https://github.com/pallets/flask) para prover o endpoint de inicialização LTI (`POST /launch`) utilizando a biblioteca [Authlib](https://github.com/lepture/authlib) como servidor OAuth1. Abaixo seguem os tópicos que serão abordados:

* Estrutura do Projeto
    * Implementação (Provedor de Ferramenta LTI)
    * Verificação OAuth1 e validação da mensagem de inicialização LTI 1.0
    * Detalhes adicionais
* Instalação
* Executando o app (Provedor de Ferramenta LTI)
* OAuth1 Server (Authlib)

### Estrutura do projeto

Essa é a árvore de arquivos desse projeto:
```
.
├── README.md
├── requirements.txt
└── src
    ├── app.py
    ├── lti1p0
    │   ├── errors.py
    │   ├── __init__.py
    │   └── provider.py
    ├── templates
    │   └── message_params.html
    └── utils
        ├── cache.py
        └── oauth1_server.py
```

No diretório `src`, o arquivo `app.py` é o código da nossa aplicação Flask, aqui está definido o endpoint de inicialização LTI (`POST /launch`) na função `launch()` e onde é realizada a verificação OAuth1 e as validações da mensagem LTI de inicialização. Na próximo item será explicado o processo e verificação OAuth1 e LTI.

No diretório `lti1p0` estão os códigos relacionados ao LTI 1.0. No arquivo `provider.py` está a implementação das validações de inicialização básica do LTI 1.0. No arquivo `errors.py` econtram-se algumas de erros relacionados ao protocolo.

Em `utils` estão os arquivos `oauth1_server.py` e `cache.py` relativos ao servidor OAuth1.

O arquivo `templates/message_params.html` é o template html utilizado nesse exemplo para exemplificar um recurso e exibir o estado da solicitação de inicialização.

### Verificação OAuth1 e validação da mensagem de inicialização LTI 1.0

No arquivo `app.py` você verifica a definição do endpoint e da função `launch()`. Nessa função é implementada a funcionalidade mais importante desse projeto: Verificação e validação da mensagem de inicialização LTI 1.0.

O código abaixo mostra uma implementação simplificada, porém funcional, da validação e verificação da mensagem LTI recebida (A seguir será explicado o passo a passo das partes mais importantes dessa função):

```python
## Código modificado para simplificação
@app.route('/launch', methods=['POST'])
def launch():
    verified = False
    try:
        # Valida requisição OAuth1
        oauth1_request = server.create_oauth1_request(request)
        server.validate_temporary_credentials_request(oauth1_request)

        # Verifica pacote de inicialização básica LTI 1.0
        launch = LTILaunchForm(request.form)
        LTILaunchForm.validate_lti_launch(launch)

        verified = True
    except (OAuth1Error, LTIError) as error:
        # Tratamento de erro
        pass

    # Prepara o recurso e a resposta da requisição
    if verified:
        # Se verificado, prepara o recurso para retornar ao usuário
        response = 'Passou! Mostrar recurso privado!'
        status_code = 200
    else:
        # Caso contrário, prepara mensagem de erro
        response = 'Autenticação falhou!'
        status_code = 401

    # Retorna resposta
    return response, status_code
```

Em resumo, a função `launch()` realiza os seguintes passos:
```
1. Verifica se a requisição OAuth1 é válida (cliente válido - hub -, assinatura válida, timestamp, nonce, etc)
2. Verifica se a requisição é uma mensagem LTI válida (lti_message_type=basic-lti-launch-request, lti_version=LTI-1p0, resource_link_id presente, etc).
3. Se válido (OAuth1 Ok e Mensagem LTI Ok),
 a. Prepara o recurso autenticado e retorna para o usuário.
 b. Caso contrário, não exibir recurso privado para o requisitante.
```

A validação OAuth1 é realizada nas seguintes linhas:
```python
# Valida requisição OAuth1
oauth1_request = server.create_oauth1_request(request)
server.validate_temporary_credentials_request(oauth1_request)
```
Aqui `oauth1_request = server.create_oauth1_request(request)` traduz a requisição recebida pelo Flask para o formato entendido pela biblioteca `Authlib`. Em `server.validate_temporary_credentials_request(oauth1_request)` faz as validações OAuth1 (cliente válido, timestamp, nonce, assinatura, etc). Caso alguma das verificações falhe, uma exceção é disparada indicando erro na verificação OAuth. Caso nenhuma exceção seja disparada, o código continua para a validação do pacote LTI.

A validação da mensagem LTI ocorre nas seguintes linhas:
```python
# Verifica pacote de inicialização básica LTI 1.0
launch = LTILaunchForm(request.form)
LTILaunchForm.validate_lti_launch(launch)
```

A linha `launch = LTILaunchForm(request.form)` realiza uma transformação do formulário da requisição Flask para um formato entendido pela biblioteca LTI implementada. `LTILaunchForm` aceita basicamente qualquer fomato que seja transformado nativamente em um dicionário como `dict(request_form)`. A linha `LTILaunchForm.validate_lti_launch(launch)` realiza as validações da mensagem LTI, basicamente lti_message_type=basic-lti-launch-request, lti_version=LTI-1p0 e resource_link_id presente. Você pode checar a implementação de `LTILaunchForm.validate_lti_launch()` em `lti1p0/provider.py` para as verificações realizadas ou customizar sua própria verificação. Caso alguma das validações falhem uma exceção é disparada indicando erro na validação LTI. Caso nenhum erro durante a validação LTI ocorra o código continua configurando o estado da variável `verified` para `True` e segue para a preparação e retorno dos recursos.

Neste código simplificado, caso a mensagem LTI seja verificada, o recurso mostrado é apenas `Passou! Mostrar recurso privado!` com código `200`, e `Autenticação falhou!` com código `401` caso contrário. No código implementado algumas informações extras são apresentadas, como os parâmetros lidos e mensagens de erros mais explicativas para facilitar testes, mas seguem o mesmo padrão apresentado aqui.

**IMPORTANTE** Fique ciente, entretanto, que a forma de apresentação dos recursos privados varia de aplicação para aplicação e que esse é somente um exemplo. É muito provável que sua aplicação tenha sua própria gestão de usuários e autenticação, neste caso você deve implementar todas as etapas necessárias para a exibição final do recurso para o aluno ou usuário, como levantamento de sessão do usuário, alocação de recursos, redirecionamentos de páginas necessários, etc.

### Detalhes adicionais

Como segue a definição, a comunicação OAuth no endpoint de inicialização necessita que o Hub seja identificado como cliente OAuth1 do servidor. Em `app.py`, adicionamos o Hub como cliente do servidor no seguinte trecho do código:

```python
# Create Hub Educacional Client Example
hub_client_example = Client(
    client_id='hub.educacional.com',
    client_secret='secret',
)

# Persist Hub client to Client Database
db.session.add(hub_client_example)
db.session.commit()
```

Essa adição foi realizada apenas para exemplo e pode variar de acordo com o servidor OAuth implementado. Você pode adicionar outros clientes da mesma forma para testes se desejar.

## Instalação
Antes de testar instale os requisitos:
```
$ python --version
Python 3.7.4
$ pip install -r requirements.txt
```

## Executando o app (Provedor de Ferramenta LTI)

Unix:
```
$ export FLASK_APP=src/app.py
$ flask run
```
ou apenas,
```
$ cd src
$ flask run
```

Windows CMD:
```
> set FLASK_APP=src/app.py
> flask run
```

Windows PowerShell:
```
> $env:FLASK_APP = src/app.py
> flask run
```

## OAuth1 Server (Authlib)

LTI 1.0 é construído sobre o padrão de segurança OAuth1 para autorização e autenticação. Nesse exemplo é utilizada a biblioteca [Authlib](https://github.com/lepture/authlib). O código do servidor OAuth1 foi retirado do próprio repositório da biblioteca.

Arquivo do servidor: `authlib/tests/flask/test_oauth1/oauth1_server.py` ([e035dc9](https://github.com/lepture/authlib/blob/e035dc958f7b7355255dba0a721e79aabd0e5c4d/tests/flask/test_oauth1/oauth1_server.py))

**IMPORTANTE:** Caso sua aplicação seja ou será escrita em outro framework, cheque a documentação da [Authlib](https://github.com/lepture/authlib) para verificar se seu framework é suportado, e os códigos de test do repositório para adaptação. Caso você já tenha seu sevidor OAuth1 configurado ou pretenda usar outra biblioteca OAuth, adicione apenas as validações LTI em um endpoint com verificação OAuth de acordo com sua implementação e seguindo as documentações LTI acima. Para outras linguagens e bibliotecas, verifique [OAuth.net](https://oauth.net/1/).